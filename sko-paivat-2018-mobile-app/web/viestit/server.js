// Sisällytetään rajapinnat
var express = require('express');
var hbs = require('hbs');
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var path = require('path');
var routes = require('./routes/index');

var app = express();
app.set('view engine', 'hbs');
app.set('views', path.join(__dirname, '/views'));
app.use(express.static(path.join(__dirname, '/public')));
app.use('/', routes);
app.enable('trust proxy');

// Server setup
var server = app.listen(5000, function(){
    var host = server.address().address
    var port = server.address().port
    console.log("App listening at ", host, port)
});

// Export server
module.exports = app;