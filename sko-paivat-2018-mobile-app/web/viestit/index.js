﻿// Dependencies
var express = require('express');
var includes = require('array-includes');
var router = express.Router();
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false });
router.use(bodyParser.json());

// Mongo Setup
var mongo = require('mongodb').MongoClient;
var objectId = require('mongodb').ObjectID;
var assert = require('assert');
var url = 'mongodb://IP-osoite';

// SESSION
var session = require('express-session');
router.use(session({ 
    secret: 'supersecretsessionkey1312', 
    cookie: { maxAge: 7 * 24 * 3600000 }
}));

// GET DATA (Etusivu)
router.get('/', function(req,res,next){ 
    var resultArray = [];
    mongo.connect(url, function(err, db){
        assert.equal(null, err);
        var cursor = db.collection('data').find().sort({$natural: -1});
        cursor.forEach(function(doc, err){
            assert.equal(null, err);
            resultArray.push(doc);
        }, function(){
            db.close();
            res.render('template', {items: resultArray});
        });
    });
});

// INSERT DATA
router.post('/insert-data', urlencodedParser, function(req,res, next){
    var message = {
        msgfield:req.body.msgfield,
        submit_time:req.body.submit_time,
        likes:req.body.likes = 0
    };
    mongo.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection('data').insertOne(message, function(err, result){
            assert.equal(null, err);
            console.log('DATA INSERTED, ' + message.submit_time + ': ' +  message.msgfield);
            db.close();
        });
    });
    res.redirect('http://IP/chatti');
});

// UPDATE DATA (Viestin tykkäys)
router.post('/like/:id', urlencodedParser, function(req, res, next){
    var msgid = req.body.id; 
    
    if (req.session.array){
        if(!includes(req.session.array, msgid)){
        req.session.array.push(msgid);                
        console.log(req.session.array);
        
        mongo.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection('data').updateOne({_id: objectId(msgid)}, {$inc: {likes: 1}}, function(err, result){
        assert.equal(null, err);
        db.close();
            });   
        }); 
    }
    else{
        console.log('You have already liked this message.');
    }
    }else{
        req.session.array = [];
        console.log('New session array created');
        req.session.array.push(msgid);
    
        mongo.connect(url, function(err, db){
        assert.equal(null, err);
        db.collection('data').updateOne({_id: objectId(msgid)}, {$inc: {likes: 1}}, function(err, result){
            assert.equal(null, err);
            db.close();
        });   
    });
}
res.redirect('http://IP/chatti');
});

module.exports = router;