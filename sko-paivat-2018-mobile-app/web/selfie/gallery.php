<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SKO-galleria</title>
    <script src="../jquery-3.2.1.js"></script>
    <link rel="stylesheet" href="stylesheet6.css">
    <link rel="stylesheet" href="../header.css">

    <style>
        #popUpImg {
            display: block;
            width: 95%;
            height: 84%;
            margin: auto;
            padding: 1% 0;
        }

        #likeButton {
            display: block;
            margin: auto;
            background-color: #c5b358;
            width: auto;
            height: 10%;
            border: none;
            border-radius: 10px;
            outline: none;
            color: white;
            white-space: normal;
        }

        #likebutton:active {
            background-color:  #910000;
        }

        #likeImg {
            width: 60%;
            height: 80%;
        }

        .pikkukuvat{
            padding: 1px;
            margin: 10px;
        }

        .pikkukuvat img{
            width: 100%;
            height: 50%;     
        }

        #osallistumisButton {
            display: block;
            padding: 1px;
            margin: 10px;
            background-color: #c5b358;
            width: auto;
            height: 20%;
            border: none;
            border-radius: 10px;
            outline: none;
            color: white;
            font-weight: bold;
            font-size: 25;
            white-space: normal;
        }

        #osallistumisbutton:active {
            background-color:  #910000;
        }

        p{
            margin-left: 15px;
        }

    </style>
</head>
<body>
    <div id="header">
        <div id="headerTextContainer">
                <div id="headerDiv">
                    <h1 id="headerText">SKO-päivät</h1>
                </div>
                <div id="headerSubDiv">
                     <h2 id="headerSubtext">Mappimerestä Digiavaruuteen</h2>
                </div>
        </div>

        <div id="infoButtonDiv">
            <a href="../index.html"><img src="../ikonit/homenappi.svg" alt="(i)" id="homeImage"></a>
        </div>
    </div>
    <div id="mainContainer">
        <button id="osallistumisButton" onclick="changePage('kuvanlahetys.html')">Osallistu selfiekilpailuun tästä!</button>

        <p>Näpäytä kuvaa katsoaksesi sitä kuvatilassa. Kuvan näpäytys kuvatilassa palauttaa galleriaan. Kuvatilassa voit tykätä valitsemastasi kuvasta.<p>

        <?php
        $dirname = "images/";
        $images = glob($dirname."*.{jpg,jpeg,png}",GLOB_BRACE);

        foreach(array_reverse($images) as $image) {
            ?>
            <div class= "pikkukuvat">
                <img src="<?php echo $image ;?>" alt="<?php echo $image ;?>" onClick ="openImage('<?php echo $image ;?>')">
            </div>
            <?php
        }
        ?>
    </div>
        <div id="popUp">

        </div>
</body>    
    <script>
        function pisteensyotto(muisti)
        {
            $(document).ready(function(){   
                $.ajax({
                    type:'POST',
                    url :"aani.php",
                    data: {muisti:muisti},
                    success: function(data) 
                    {
                        annetutaanet(muisti);
                    }
                }); 
            });
        }        

        var muisti='';
        var loytyy=0;

        var omalista = [localStorage.getItem("lista")];
        omalista = JSON.parse(omalista);
   

        function muistiin()
        {
            localStorage.setItem('lista', JSON.stringify(omalista))
        }

        function openImage(url) {
            document.getElementById("popUp").innerHTML = `
            <img id="popUpImg" onclick="closeImage()"><br>
            <button type= "button" name= "submit" id="likeButton" onClick="annetutaanet(muisti)"><img src="../ikonit/peukku2.svg" id="likeImg"></button>
            `;  
            muisti = url.slice(7);
            document.getElementById('popUpImg').src=url;
            document.getElementById("popUp").style.zIndex = 1;
            document.getElementById("mainContainer").style.filter = "blur(3px)";
            document.getElementById("header").style.filter = "blur(3px)";
        }

        function closeImage() {
            document.getElementById("popUp").innerHTML = "";
            document.getElementById("mainContainer").style.filter = "none"; 
            document.getElementById("header").style.filter = "none";
            document.getElementById("popUp").style.zIndex = -1;
        }

        function changePage(url) {
            window.location.href = url;
        }

        function annetutaanet(muisti) {
            loytyy=0;
            omalista.sort();
            for (i=0; i< omalista.length; i++){
                (function (i) {
                    if (omalista[i]==muisti){
                        loytyy=1;
                    }
                })(i);
            }
            if(loytyy==0) {
                omalista.push(muisti);
                pisteensyotto(muisti);  
            }
            muistiin();   
        } 
    </script>
</html>