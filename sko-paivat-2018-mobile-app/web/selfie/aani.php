<?php

if (isset($_POST['muisti'])){
	$nimi = $_POST['muisti'];

	if (file_exists('/Address/selfie/pisteet.xml')) {
		$xml = simplexml_load_file('/Address/selfie/pisteet.xml');
		if (is_writable('/Address/selfie/pisteet.xml')) {

			if(!empty($nimi))
			{
				foreach ($xml->kuva as $valittu) 
				{
					if ($valittu->nimi == $nimi){
						$uudetpisteet= (int) $valittu->pisteet;
						$uudetpisteet++;
						$valittu->pisteet=$uudetpisteet; 
						break; 
					}
				}
			}
			
			file_put_contents ($xml->asXML('/Address/selfie/pisteet.xml'));
			exit();	
		}
		else {
			echo 'Tiedostoon ei voi kirjoittaa!';
		}
	}
	else {
		echo 'Tiedostoon ei voi kirjoittaa!';
	}
}

?>