<html>
    <head>
    <meta charset="UTF-8">
	    <meta name="viewport"
	         content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	    <meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title> Viestit -SKO päivät </title>
    <link rel="stylesheet" type="text/css" href="/~sku/viestit/public/stylesheet.css">
    <link rel="stylesheet" type="text/css" href="/~sku/viestit/public/header.css">
    <script type="text/javascript" src="../javascript.js"></script>
    <title>SKO-viestit</title>

    <style>
    table, th, td {
      border: 1px solid black;
      border-collapse:collapse;
    }
    th, td {
      padding: 5px;
    }
    </style>

    </head>
    <body>
    	<form action="lisaakuvajapisteet.php" method="POST" enctype="multipart/form-data">
            <input type="file" name="file" id="file" style= "float: left;">
            <button type="submit" name= "submit"> Lähetä kuva </button>
        </form>

        <button type="button" onclick="loadXMLDoc()">haetiedot</button>
        <br><br>
        <table id="demo"></table>
    <script>

        function loadXMLDoc() {
          var xmlhttp = new XMLHttpRequest();
          xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
              myFunction(this);
            }
          };
          xmlhttp.open("GET", "pisteet.xml", true);
          xmlhttp.send();
        }

        function myFunction(xml) {
          var i;
          var xmlDoc = xml.responseXML;
          var table="<tr><th>kuva</th><th>pisteet</th></tr>";
          var x = xmlDoc.getElementsByTagName("kuva");
          for (i = 0; i <x.length; i++) { 
            table += "<tr><td>" +
            x[i].getElementsByTagName("nimi")[0].childNodes[0].nodeValue +
            "</td><td>" +
            x[i].getElementsByTagName("pisteet")[0].childNodes[0].nodeValue +
            "</td></tr>";
          }
          document.getElementById("demo").innerHTML = table;
        }
     </script>

    </body>
</html>