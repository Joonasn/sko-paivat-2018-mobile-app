<?php

if (isset ($_POST['submit'])){
	$file= $_FILES['file'];
	$timestamp = date("H:i:s");

	$fileName = $_FILES['file']['name'];
	$fileTMPName = $_FILES['file']['tmp_name'];
	$fileSIZE = $_FILES['file']['size'];
	$fileERROR = $_FILES['file']['error'];
	$fileTYPE = $_FILES['file']['type'];

	$fileExt = explode('.',$fileName);
	$fileActualExt = strtolower(end($fileExt));

	$allowed = array('jpg', 'jpeg', 'png');

	if (in_array($fileActualExt, $allowed)){
		if ($fileERROR===0){
				$fileNameNew = $timestamp.'.'.uniqid('',true).'.'.$fileActualExt;
				$fileDestination = 'images/'.$fileNameNew;
				move_uploaded_file($fileTMPName, $fileDestination);

				if (file_exists('/home/sku/public_html/selfie/pisteet.xml')) {
				    $xml = simplexml_load_file('/home/sku/public_html/selfie/pisteet.xml');

				    if (is_writable('/home/sku/public_html/selfie/pisteet.xml')) {
    	
					    $kuva = $xml->addChild('kuva');
						$kuva->addChild('nimi', $fileNameNew);
						$kuva->addChild('pisteet', '0');

						file_put_contents ($xml->asXML('/home/sku/public_html/selfie/pisteet.xml'));
						header("Location: http://IP/selfie/gallery.php");
						exit();	

					} else {
					    echo 'Tiedostoon ei voi kirjoittaa!';
					}

				} else {
				    exit('xml tiedostoa ei saatu auki');
				}

		}else{
			echo "Virhe lahettaessa tiedostoa. Ehka tiedosto on liian suuri? Yrita uudelleen!";
		}
	}else{
		echo "Vain jpg, jpeg ja png -tiedostot tuettu!";
	}
}

?>