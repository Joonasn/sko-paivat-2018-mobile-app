SKO- Päivät 2018 Mobiilisovellus
-------------------------------------------------------
Opiskelijaprojekti ja toimeksianto Joensuussa vuonna 2018 järjestetyn SKO -Päivät tapahtuman järjestäjiltä.
Mobiilisovelluksen periaatteena oli toimia yhdessä sovellusta varten asetetun palvelimen kanssa, joka sisälsi suurimman osan sovelluksen funktionaalisuudesta. 
Tämän toteutustavan ansiosta projektia pystyi päivittämään kaikille käyttäjille ilman sovelluksen päivittämistä.

Sovelluksen sisältöön kuului tapahtuman kartta- ja kalenteritiedot, viestintäominaisuus sekä galleria tapahtuman "selfiekilpailua" varten. 
Gallerian kuvia pystyi myös äänestämään.

Projektin Android-versio päätyi julkaisuun asti. Myös IOS-versio oli työn alla.


Tiedostot
---------------------------------------------------

Projekti on kokoelma sovelluksesta käytetyistä tiedostoista.
Julkaistu projektin toimeksiantajien luvalla.
